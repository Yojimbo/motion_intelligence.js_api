function MI(){

    var that            = this;
    that.map            = undefined;
    that.sliderControl  = undefined;
    that.sources        = new Array();
    that.sourcePoints   = '';
    that.targets        = new Array();
    that.targetPoints   = '';
    that.points         = '';
    that.routes         = '';
    that.cpl            = 'undefined';
    that.startTime      = getStartTime();
    that.travelTimes    = config.travelTimes;
    that.travelSpeed    = config.defaultTravelSpeed;
    that.callback       = undefined;

    that.init = function(map, options) {

        getStartTime();
        that.map = map;
        that.initializeMap(options);
        // if a map was given, we can't start to get the polygons
        // right away, we have to add markers first
        if (typeof map === 'undefined' && that.sources.length > 0) that.getTravelTimePolygons();
    }

    that.getMap = function() {

      return that.map;
    }

    that.getRoutesLayer = function() {

        return that.routes;
    }

    that.getTravelTimes = function() {

        return that.sliderControl.getValues();
    }

    that.getCurrentTravelTime = function() {

        return that.sliderControl.getValue();
    }

    /**
     * Initializes the map on the div "map". This method can be configured
     * to use maps from either Cloudmade or MapBox. Furthermode the config
     * can be used to specify the initial gps coordinate, init/min/max zoom
     * levels and the map attribution. 
     * This method also adds a CustomPolygonLayer to the map.
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.initializeMap = function(options) {

        // map already available, we don't need to create a new one
        if ( typeof that.map === 'undefined' ) {

            // create the map
            that.map = L.map(config.mapDiv, { center : config.gpsStartPoint });

            // and add a tile layer
            var tileUrl = '';

            if ( config.cloudmade.active ) {
                
                tileUrl = config.cloudmade.url;
                tileUrl = tileUrl.replace("API_KEY", config.cloudmade.apiKey);
                tileUrl = tileUrl.replace("MAP_STYLE", config.cloudmade.mapStyle);
            }
            else if ( config.mapbox.active && !config.cloudmade.active ) {

                tileUrl = config.mapbox.url;
                tileUrl = tileUrl.replace("MAP_STYLE", config.mapbox.mapStyle);
            }
            else alert("Neither Cloudmade nor MapBox. Have a look at the config!");
            if ( config.mapbox.active && config.cloudmade.active ) alert('MapBox and Cloudmade actived');

            // create tile layer and add it to the map
            L.tileLayer(tileUrl, { 
                minZoom : config.minZoom, 
                maxZoom: config.maxZoom, 
                attribution: config.attribution
            }).addTo(that.map);
        }
                    
        // only load slider if wanted
        if ( options.addTravelTimeSlider ) {
            that.sliderControl = new SliderControl(options);
            that.map.addControl(that.sliderControl);
            // that.sliderControl.startSlider(options);
            that.sliderControl.onSlideStop(function (travelTimes){
                $("#waitControl").show();
                that.travelTimes = travelTimes;
                that.getTravelTimePolygons(that.sources, travelTimes, that.travelMode, { date: options.date, time : options.time});
                return travelTimes;
            });
        }

        that.waitControl = new WaitControl({});
        that.map.addControl(that.waitControl);

        // // create a layer to group all source points
        that.sourcePoints = L.featureGroup().addTo(that.map);
        that.targetPoints = L.featureGroup().addTo(that.map);
        that.routes       = L.featureGroup().addTo(that.map);
        that.points       = L.featureGroup();

        // add default marker from the config file
        _.each(config.defaultMarkers, function(marker, index){

            var marker = L.marker(marker.latlon, {draggable: marker.draggable}).addTo(that.sourcePoints);
            marker.id = index;

            // cache the markers
            that.sources.push(marker);
            // recalculate polygons after each movement
            marker.on("dragend", function(e){
                that.getTravelTimePolygons();
                that.getRoutes();
            });
        });
        
        // adjust the map to all points in the map but with min zoom
        // if no point was added so far we can't calculate the initial view area
        var center = !that.sourcePoints.getBounds().isValid() ? config.gpsStartPoint : that.sourcePoints.getBounds().getCenter();
        that.map.setView(center, config.initZoom);
        
        // we need to add a custom polygon in order to draw svg/vml
        // polygons which overlap and to not mix overlapping colors
        that.cpl = new CustomPolygonLayer();
        that.map.addLayer(that.cpl);

        // if we click on the map we want to get routes
        if ( config.clickMapForRoutes ) that.map.on('contextmenu', that.clickAddMarker);
    }

    that.clickAddMarker = function(event) {

        return that.addTarget(event.latlng, {id : "test"});
    }

    that.clearPoints = function() {

        that.targetPoints.clearLayers();
        
    }

    that.getTargetPoints = function() {

        return that.targetPoints;
    }

    /**
     * 
     */
    that.addSource = function(sourceMarker, options, callback) {

        $("#waitControl").show();

        // add the marker to the target layer (and so to the map)
        sourceMarker.addTo(that.map);
        sourceMarker.addTo(that.points);
        sourceMarker.id = options.id; 
        sourceMarker.bindPopup(options.html);
        sourceMarker.on("dragend", function(e){
            $("#waitControl").show();
            that.getTravelTimePolygons(undefined,undefined,undefined, options);
            $("#waitControl").show();
            that.getRoutes(undefined, undefined, options);
        });

        // cache the markers
        if ( !_.contains(that.sources, sourceMarker) ) that.sources.push(sourceMarker);

        options.callback = callback;
        that.callback = callback;
        
        // recalculate the routes to the targets
        that.getRoutes(undefined, undefined, options);
        if (options.addTravelTimePolygons) that.getTravelTimePolygons(undefined, undefined, undefined, options);
    }

    that.addTarget = function(latlng, options) {

        // remove everthing if wanted
        if ( options.clear ) {
            that.points.clearLayers();
            that.targets = [];
            that.routes.clearLayers();
        }//that.clearPoints();

        // add the marker to the target layer (and so to the map)
        var targetMarker = L.marker(latlng, options);
        // if we already have markers in the map we don't want to add it again
        if ( options.addToMap ) targetMarker.addTo(that.targetPoints);
        // define additional behaviour
        targetMarker.id = options.id; 
        targetMarker.on("dragend", function(e){
            that.getRoutes(that.sources, that.targets);
        });
        targetMarker.addTo(that.points);

        // cache the markers
        if ( !_.contains(that.targets, targetMarker) ) that.targets.push(targetMarker);

        // recalculate the routes to the targets
        return that.getRoutes(that.sources, that.targets, options);
    }

    that.getRouteTime = function(sources, targets, options, callback) {

        var cfg = {};
        cfg.sources = [];
        cfg.targets = [];
        cfg.pathSerializer = 'compact';

        if ( options.maxRoutingTime ) cfg.maxRoutingTime = options.maxRoutingTime;

        _.each(sources, function(source){

            var src = {};
            src.id  = source.id;
            src.lat = source.lat;
            src.lon = source.lon;
            src.tm = {};
            src.tm.transit = {};
            src.tm.transit.frame = {};
            src.tm.transit.frame.time = source.time;
            src.tm.transit.frame.date = source.date;
    
            cfg.sources.push(src);
        });
        
        _.each(targets, function(target) {

            var trg = {};
            trg.id  = target.id;
            trg.lat = target.lat;
            trg.lon = target.lon;
            cfg.targets.push(trg);
        });

        $.ajax({
            url:         config.serviceUrl + config.serviceVersion + '/time',
            type:        "POST",
            data:        JSON.stringify(cfg),
            contentType: "application/json",
            dataType:    "json",
            success: function (result) {
                callback(result);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    }

    /**
     *
     */
    that.getRoutes = function(sources, targets, options) {

        var sources = typeof sources !== 'undefined' ? sources : that.sources;
        var targets = typeof targets !== 'undefined' ? targets : that.targets;
        var travelMode  = typeof travelMode  !== 'undefined'  ? travelMode  : typeof that.travelModeControl !== 'undefined' ? that.travelModeControl.getValue() : config.travelMode; 

        var startTime = that.startTime;
        var date = that.startDate;

        // if there are no target points available, no routing is possible! 
        if ( sources.length != 0 && targets.length != 0 ) {

            var cfg = {};
            cfg.sources = [];
            _.each(sources, function(source){

                var src = {};
                src.id  = source.id;
                src.lat = source.getLatLng().lat;
                src.lon = source.getLatLng().lng;
                src.tm = {};
                src.tm[travelMode] = {};
                if(travelMode == "transit"){
                    src.tm.transit.frame = {};
                    src.tm.transit.frame.time = options.time;
                    src.tm.transit.frame.date = options.date;
                    // src.tm.transit.frame.date = date;
                }
                cfg.sources.push(src);
            });
            cfg.targets = [];
            _.each(targets, function(target){

                var trg = {};
                trg.id  = target.id;
                trg.lat = target.getLatLng().lat;
                trg.lon = target.getLatLng().lng;
                cfg.targets.push(trg);
            });

            return $.getJSON(config.serviceUrl + config.serviceVersion + '/route?cfg=' + 
                encodeURIComponent(JSON.stringify(cfg)) + "&cb=?", 
                    function(json){

                        console.log(json);

                        that.routes.clearLayers();

                        _.each(json.routes, function(route, index){

                            _.each(route.segments, function(segment, index){

                                var color = "green";
                                var transitLi ='';
                                if(segment.isTransit){
                                  color = _.findWhere(config.routeTypes, {routeType : segment.routeType}).color;
                                  transitLi +=  "<li><span lang='de'>Line: " + segment.routeShortName + " departure: </span>" + that.secondsToString(segment.departureTime) + " arrival: " +  that.secondsToString(segment.arrivalTime) + "</li>";
                                }

                                // build the line
                                var points = []
                                _.each(segment.points, function(point) { points.push(point); });

                                var polylineOptions       = {};
                                polylineOptions.color     = color;

                                var polylineHaloOptions = {};
                                polylineHaloOptions.weight = 7;
                                polylineHaloOptions.color     = "white";
                                
                                // the first and the last segment is walking so we need to dotted lines
                                if ( index == 0 || index == (route.segments.length - 1) ) polylineOptions.dashArray = "1, 8";
                                
                                L.polyline(points, polylineHaloOptions).addTo(that.routes);  
                                var routeLine = L.polyline(points, polylineOptions).addTo(that.routes);

                                if ( options.addPopup ) {

                                    routeLine.bindPopup(
                                        "<ul>" +
                                          "<li><span lang='de'>Von: </span>" + segment.startname + " nach: " +  segment.endname + "</li>" +
                                           transitLi +
                                          "<li><span lang='de'>Reisedauer: </span>" + (segment.traveltime / 60).toFixed(0) + "min von: " + (route.traveltime / 60).toFixed(0) + "</li>" +
                                          "<li><span lang='de'>Länge: </span>" + segment.length.toFixed(1) + "km</li>" +
                                          "<li><span lang='de'>Höhenunterschied: </span>" + segment.elevationGain.toFixed(0) + "m</li>" +
                                       "</ul>");
                                }

                                if ( options.popup ) {
                                    
                                    routeLine.bindPopup(options.popup);
                                    routeLine.on('click', function(){
                                        $('.poi-expose-popup-'+ options.poiId).html(secToHoursMin(json.routes[0].traveltime));
                                    });
                                }
                            })
                        });

                        if (options.fitBounds) that.map.fitBounds(that.points.getBounds(), {padding : [100,100]});
                    });
        }
    }

    that.secondsToString = function(seconds){
      var hours = Math.floor(seconds/3600);
      var minutes = Math.floor(seconds/60)-hours*60;
      seconds = seconds - (hours * 3600) - (minutes *60);
      return hours+":"+ ("0" + minutes).slice(-2) +":"+ ("0" + seconds).slice(-2);
    },

    /**
     * 
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getTravelTimePolygons = function(sources, travelTimes, travelMode, options) {
        
        var configTravelTimes = [];
        _.each(config.travelTimes, function(cfgTime) { configTravelTimes.push(cfgTime.time); });

        var sources     = typeof sources     !== 'undefined'  ? sources     : that.sources;
        var travelTimes = typeof travelTimes !== 'undefined'  ? travelTimes : typeof that.sliderControl !== 'undefined' ? that.sliderControl.getValues() : configTravelTimes;
        var travelMode  = typeof travelMode  !== 'undefined'  ? travelMode  : typeof that.travelModeControl !== 'undefined' ? that.travelModeControl.getValue() : config.travelMode; 

        var startTime = that.startTime;
        var date = that.startDate;
        var speed = that.travelSpeed;

        var cfg = {};
        cfg.polygon = { values : travelTimes };
        cfg.sources = [];
        _.each(sources, function(source){

            var src = {};
            src.id = source.id;
            src.lat = source.getLatLng().lat;
            src.lon = source.getLatLng().lng;
            src.tm = {};           
            src.tm[travelMode] = {};
            if(travelMode == "transit"){
                src.tm.transit.frame = {};
                src.tm.transit.frame.time = options.time;
                src.tm.transit.frame.date = options.date;
            }
            if(travelMode == "bike"){
                src.tm.bike.speed = speed;
            }
            cfg.sources.push(src);
        });

        $.getJSON(config.serviceUrl + config.serviceVersion + '/polygon?cfg=' + encodeURIComponent(JSON.stringify(cfg)) + "&cb=?", that.loadPolygons).
            success(function(polygons){

                if (that.callback) that.callback(polygons);
                $("#waitControl").hide();
            });
    }

    /**
     * 
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.loadPolygons = function(json) {

        var polygons = that.parsePolygons(json);
        that.cpl.addLayer(polygons);
        that.map.fitBounds(that.cpl.getBoundingBox());

        return polygons;
    }

    /**
     * 
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.parsePolygons = function(polygonsJson) {
        
        var that = this;
        
        var polygons = new Array();

        if ( polygonsJson.error ) return errorMessage;

        _.each(polygonsJson["polygons"], function (polygonJson) {

            var polygon = new Polygon();
            polygon.travelTime = polygonJson.traveltime;
            polygon.setOuterBoundary(that.parseLatLonArray(polygonJson.outerBoundary));
            polygon.setBoundingBox();

            _.each(polygonJson.innerBoundary, function (innerBoundary) {
                polygon.addInnerBoundary(that.parseLatLonArray(innerBoundary));
            });
            
            polygons.push(polygon);
        });

        return polygons;
    }

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.parseLatLonArray = function(latlngs) {

        var coordinates = new Array();

        _.each(latlngs, function (latlng) {
            coordinates.push(L.latLng(latlng[0], latlng[1]));
        });

        return coordinates;
    }

    /**
     * Not needed anymore
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.timeToCalories = function(travelTime, cyclingSpeed){

        if ( cyclingSpeed == "slowCycling" ) {
            
            return travelTime * 0.081;
        }
        if ( cyclingSpeed == "mediumCycling" ) {

            return travelTime * 0.163;
        }
        if ( cyclingSpeed == "fastCycling" ) {
            
            return travelTime * 0.245;
        }
    }
}

/**
 *
    @DANIEL: WHY not in MI namespace?

 * @author Daniel Gerber <daniel.gerber@icloud.com>
 * @author Henning Hollburg <henning.hollburg@gmail.com>
 */
CustomPolygonLayer = L.Class.extend({

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    initialize: function () {
        // save position of the layer or any options from the constructor
        this._latlng = new L.LatLng(-180, 90);
        this._topRight = new L.latLng(-90,-180);
        this._bottomLeft = new L.latLng(90, 180);
        this._multiPolygons = new Array();
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    getBoundingBox : function(){
        return new L.LatLngBounds(this._bottomLeft, this._topRight)
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    onAdd: function (map) {

        this._map = map;
        // create a DOM element and put it into one of the map panes
        this._el = L.DomUtil.create('div', 'my-custom-layer-'+$(map._container).attr("id")+' leaflet-zoom-hide');
        $(this._el).css({"opacity":0.4});
        $(this._el).attr("id","canvas" + $(this._map._container).attr("id"));
        this._map.getPanes().overlayPane.appendChild(this._el);

        // add a viewreset event listener for updating layer's position, do the latter
        this._map.on('viewreset', this._reset, this);
        this._reset();
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    addLayer:function(polygonArray){
        
        this.initialize();
        for(var i = 0; i < polygonArray.length; i++){
                this._updateBoundingBox(polygonArray[i].outerBoundary);
                this._addPolygonToMultiPolygon(polygonArray[i]);
            }
        this._multiPolygons.sort(function(a,b) { return (b.getTravelTime() - a.getTravelTime()) });
        this._reset();
    },

    _addPolygonToMultiPolygon: function(polygon){
        for(var i = 0; i < this._multiPolygons.length; i++){
            if(this._multiPolygons[i].getTravelTime() == polygon.travelTime ){
                this._multiPolygons[i].addPolygon(polygon);
                return;
            }
        }
        var mp = new MultiPolygon();
        mp.setTravelTime(polygon.travelTime);
        mp.addPolygon(polygon);
        this._multiPolygons.push(mp);
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    _updateBoundingBox:function(CoordinateArray){
        for(var i = 0; i < CoordinateArray.length; i++){
            var ll = CoordinateArray[i];
            if(ll.lat > this._topRight.lat)
                this._topRight.lat = ll.lat;
            if(ll.lat < this._bottomLeft.lat)
                this._bottomLeft.lat = ll.lat;
            if(ll.lng > this._topRight.lng)
                this._topRight.lng = ll.lng;
            if(ll.lng < this._bottomLeft.lng)
                this._bottomLeft.lng = ll.lng;
        }
        if(this._latlng.lat < this._topRight.lat)
            this._latlng.lat = this._topRight.lat;
        if(this._latlng.lng > this._bottomLeft.lng)
            this._latlng.lng = this._bottomLeft.lng;
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    onRemove: function (map) {
        // remove layer's DOM elements and listeners
        map.getPanes().overlayPane.removeChild(this._el);
        map.off('viewreset', this._reset, this);
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    _buildString:function(path, point, suffix){
        path += suffix + point.x + ' ' + point.y;
        return path;
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    _createSVGData: function(polygon){
        pathData ='';
        var point = this._map.latLngToLayerPoint(polygon[0]);
        pathData = this._buildString(pathData, point, 'M')
        for(var i = 1; i < polygon.length; i++){
            var point = this._map.latLngToLayerPoint(polygon[i]);
            pathData = this._buildString(pathData, point, 'L')
        }
        pathData += 'z ';
        return pathData;
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    clearLayers: function(){
        $('#canvas'+ $(this._map._container).attr("id")).empty();
        this.initialize();
    },

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    _reset: function () {

        if(this._multiPolygons.length > 0){
            var pos = this._map.latLngToLayerPoint(this._latlng);

            //internalSVGOffset is used to have a little space between geometries and svg frame. otherwise buffers won't be displayed at the edges...
            var internalSVGOffset = 100;
            pos.x -= internalSVGOffset;
            pos.y -= internalSVGOffset;
            L.DomUtil.setPosition(this._el, pos);

            //ie 8 and 9 
            if (navigator.appVersion.indexOf("MSIE 9.") != -1 )  {
                        $('#canvas'+ $(this._map._container).attr("id")).css("transform", "translate(" + pos.x + "px, " + pos.y + "px)");
            }
            if(navigator.appVersion.indexOf("MSIE 8.") != -1){
                        $('#canvas'+ $(this._map._container).attr("id")).css({"margin-left": pos.x + "px", "margin-top": pos.y + "px"});
            }
            $('#canvas'+ $(this._map._container).attr("id")).empty();

         

            var bottomLeft = this._map.latLngToLayerPoint(this._bottomLeft);
            var topRight = this._map.latLngToLayerPoint(this._topRight);


            var paper = Raphael('canvas'+ $(this._map._container).attr("id"), (topRight.x - bottomLeft.x) + internalSVGOffset * 2, (bottomLeft.y - topRight.y) + internalSVGOffset * 2);
            var st = paper.set();

            var svgDataArray = new Array();
            for(var i = 0; i < this._multiPolygons.length; i++){
                var mp = this._multiPolygons[i];
                var svgData = "";

                for(var j = 0; j < mp.polygons.length; j++){
                        var poly = mp.polygons[j];
                        svgData += this._createSVGData(poly.outerBoundary);
                        for(var k = 0; k < poly.innerBoundaries.length; k++){
                            svgData += this._createSVGData(poly.innerBoundaries[k]);
                        }
                        var pointTopRight = this._map.latLngToLayerPoint(poly.topRight);
                        var pointBottomLeft = this._map.latLngToLayerPoint(poly.bottomLeft);
                    }
                    // ie8 (vml) gets the holes from smaller polygons
                    if(navigator.appVersion.indexOf("MSIE 8.") != -1){
                        if(i < this._multiPolygons.length-1){
                            for(var l = 0; l < this._multiPolygons[i+1].polygons.length; l++){
                                var poly2 = this._multiPolygons[i+1].polygons[l];
                                svgData += this._createSVGData(poly2.outerBoundary);
                            }
                        
                    }
                }
                var color = mp.getColor(mp.getTravelTime());
                // var path = paper.path(svgData).attr({fill: color, stroke: color, "stroke-width":"15", "stroke-linejoin":"round","stroke-linecap":"round","fill-rule":"evenodd"})
                //             .animate(poly.travelTime/3, '-', 0)
                //             .attr({"opacity":"1"})
                //             path.translate((bottomLeft.x - internalSVGOffset) *-1,((topRight.y - internalSVGOffset)*-1));
                var path = paper.path(svgData).attr({fill: color, stroke: color, "stroke-width": "10", "stroke-linejoin":"round","stroke-linecap":"round","fill-rule":"evenodd"})
                            .attr({"opacity":"0"}).animate({ "opacity" : "1" }, poly.travelTime/3)
                            path.translate((bottomLeft.x - internalSVGOffset) *-1,((topRight.y - internalSVGOffset)*-1));
                st.push(path);
            }

            if(navigator.appVersion.indexOf("MSIE 8.") != -1){
                $('shape').each(function() {
                    $( this ).css( {"filter": "alpha(opacity=40)"} );
                });
            }

        }
    }
});

/**
 *
 * @author Daniel Gerber <daniel.gerber@icloud.com>
 * @author Henning Hollburg <henning.hollburg@gmail.com>
 */
Polygon = function(traveltime, outerBoundary) {

    var that = this;
    
    // default min/max values
    that.topRight         = new L.latLng(-90,-180);
    that.bottomLeft       = new L.latLng(90, 180);
    that.centerPoint      = new L.latLng(0,0);

    that.travelTime       = traveltime;
    that.outerBoundary    = outerBoundary;
    that.innerBoundaries  = new Array();

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.setOuterBoundary = function(outerBoundary){

        that.outerBoundary = outerBoundary;
    }

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.addInnerBoundary = function(innerBoundary){

        that.innerBoundaries.push(innerBoundary);
    }

    /**
     * @return {LatLngBounds} the leaflet bounding box
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getBoundingBox = function(){

        return new L.LatLngBounds(this._bottomLeft, this._topRight)
    }

    /**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.setBoundingBox = function() { 

        // calculate the bounding box
        _.each(this.outerBoundary, function(coordinate){

            if ( coordinate.lat > that.topRight.lat )   that.topRight.lat   = coordinate.lat;
            if ( coordinate.lat < that.bottomLeft.lat ) that.bottomLeft.lat = coordinate.lat;
            if ( coordinate.lng > that.topRight.lng )   that.topRight.lng   = coordinate.lng;
            if ( coordinate.lng < that.bottomLeft.lng ) that.bottomLeft.lng = coordinate.lng;
        });

        // precompute the polygons center
        that.centerPoint.lat = that.topRight.lat - that.bottomLeft.lat;
        that.centerPoint.lon = that.topRight.lon - that.bottomLeft.lon;
    }

    /**
     * Returns the center for this polygon. More precisly a gps coordinate
     * which is equal to the center of the polygons bounding box.
     * @return {latlng} gps coordinate of the center of the polygon
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getCenterPoint = function(){
        return that.centerPoint;
    },

    /**
     * This methods determines the color for a polygon by its travel time.
     * Colors are defined in the config file.
     *
     * @param {polygon} a polygon for which the color should be looked up by the traveltime
     * @returns {string} the hex value of the color for the polygon 
     * 
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getColor = function(time){

        var color = "NA";
        color = _.where(config.travelTimes, {time: that.travelTime}).time;
        if ( color == "NA" ) console.log("No color found for travel time: " + time);

        return color;
    }
}

MultiPolygon = function(){
    
    var that = this;    

    that._topRight = new L.latLng(-90,-180);
    that._bottomLeft = new L.latLng(90, 180);
    that.travelTime;
    that.polygons = new Array();

    /**
     * 
     */
    that.addPolygon = function(polygon){
        that.polygons.push(polygon);
    }

    /**
     * 
     */
    that.getTravelTime = function(){
        return that.travelTime;
    }

    /**
     * 
     */
    that.setTravelTime = function(travelTime){
        that.travelTime = travelTime;
    }

    /**
     * 
     */
    that.getBoundingBox = function(){
        return new L.LatLngBounds(that._bottomLeft, that._topRight)
    }

    /**
     * 
     */
    that.setBoundingBox = function(){
        for(var i = 0; i < that.polygons.length; i++){
            var poly = that.polygons[i];
            if(poly._topRight.lat > that._topRight.lat)
                that._topRight.lat = poly._topRight.lat;
            if(poly._bottomLeft.lat < that._bottomLeft.lat)
                that._bottomLeft.lat = poly._bottomLeft.lat;
            if(poly._topRight.lng > that._topRight.lng)
                that._topRight.lng = poly._topRight.lng;
            if(poly._bottomLeft.lng < that._bottomLeft.lng)
                that._bottomLeft.lng = poly._bottomLeft.lng;
        }
    }

    /**
     * that methods determines the color for a polygon by its travel time.
     * Colors are defined in the config file.
     *
     * @param {polygon} a polygon for which the color should be looked up by the traveltime
     * @returns {string} the hex value of the color for the polygon 
     * 
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getColor = function(time){

        color = _.where(config.travelTimes, {time: that.travelTime})[0].color;
        if ( typeof color === 'undefined' ) console.log("No color found for travel time: " + time);

        return color;
    }
}

WaitControl = function (options) {
    return new L.Control.WaitControl(options);
};

L.Control.WaitControl = L.Control.extend({
    options: {
        position: 'topright',
        dateFormat: "yy-mm-dd"
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
    },

    onAdd: function (map) {
        this.options.map = map;
        this.options.mapId = $(map._container).attr("id");
       
        var waitContainer = L.DomUtil.create('div', 'leaflet-control-wait');
        $(waitContainer).append(
            '<div id="waitControl" class="leaflet-time-slider waitControl">' +
                '<i class="fa fa-spinner fa-spin"></i> Bitte warten' +
            '</div>');
            
        return waitContainer;
    }
});



SliderControl = function (options) {
    return new L.Control.SliderControl(options);
};

L.Control.SliderControl = L.Control.extend({
   
    initialize: function (sliderOptions) {
        
        this.options = config.defaultTravelTimeSliderOptions;

        if(typeof sliderOptions != "undefined"){
            if ( typeof sliderOptions.position      != "undefined") this.options.position    = sliderOptions.position;
            if ( typeof sliderOptions.initValue     != "undefined") this.options.initValue   = sliderOptions.startTravelTime;
            if ( typeof sliderOptions.label         != "undefined") this.options.label       = sliderOptions.label;
            if ( typeof sliderOptions.travelTimes   != "undefined") this.options.travelTimes = sliderOptions.travelTimes;
            if ( typeof sliderOptions.icon          != 'undefined') this.options.icon        = sliderOptions.icon;
        }
        this.options.maxValue = config.defaultTravelTimeSliderOptions.travelTimes[config.defaultTravelTimeSliderOptions.travelTimes.length-1].time/60;
        this.options.step = (config.defaultTravelTimeSliderOptions.travelTimes[1].time - config.defaultTravelTimeSliderOptions.travelTimes[0].time)/60;
        this.options.initValue = sliderOptions.startTravelTime;
        this.options.refreshUrlCallback = sliderOptions.refreshUrlCallback;
        L.Util.setOptions(this);
    },

    onAdd: function (map) {
        var that = this;
        this.options.map = map;
        map.on("resize", this.onResize.bind(this));          

        var sliderColors = "";
        var percent = 100 / this.options.travelTimes.length;
        for(var i = 0; i < this.options.travelTimes.length; i++){
            if(i == 0)
                sliderColors += '<div style="position: absolute; top: 0; bottom: 0; left: ' + i * percent + '%; right: ' + (100 - (i + 1)* percent )+ '%; background-color: ' + this.options.travelTimes[i].color + '; -moz-border-top-left-radius: 8px;-webkit-border-radius-topleft: 8px; border-top-left-radius: 8px; -moz-border-bottom-left-radius: 8px;-webkit-border-radius-bottomleft: 8px; border-bottom-left-radius: 8px;"></div>';
            else if(i < this.options.travelTimes.length -1)
                sliderColors += '<div style="position: absolute; top: 0; bottom: 0; left: ' + i * percent + '%; right: ' + (100 - (i + 1)* percent )+ '%; background-color: ' + this.options.travelTimes[i].color + ';"></div>';
            else if(i == this.options.travelTimes.length -1)
                sliderColors += '<div style="position: absolute; top: 0; bottom: 0; left: ' + i * percent + '%; right: ' + (100 - (i + 1)* percent )+ '%; background-color: ' + this.options.travelTimes[i].color + '; -moz-border-top-right-radius: 8px;-webkit-border-radius-topright: 8px; border-top-right-radius: 8px; -moz-border-bottom-right-radius: 8px;-webkit-border-radius-bottomright: 8px; border-bottom-right-radius: 8px;"></div>';
        }

        // Create a control sliderContainer with a jquery ui slider
        this.options.sliderContainer = L.DomUtil.create('div', this._container);

        this.options.miBox = $('<div/>', {"class" : "leaflet-time-slider"});
        this.options.travelTimeInfo = $('<div/>');
        this.options.travelTimeSlider = $('<div/>', {"class" : "no-border"}).append(sliderColors);
        var travelTimeSliderHandle = $('<div/>', {"class" : "ui-slider-handle"});
        this.options.labelSpan = $('<span/>', {"text" : this.options.label + " "});

        if(this.options.icon != 'undefined')
            this.options.iconHTML = $('<img/>', {"src" : this.options.icon})


        this.options.travelTimeSpan = $('<span/>', {"text" : this.options.initValue });
        var unitSpan = $('<span/>', {"text" : "min"});

        $(this.options.sliderContainer).append(this.options.miBox);
        this.options.miBox.append(this.options.travelTimeInfo);
        this.options.miBox.append(this.options.travelTimeSlider);
        this.options.travelTimeSlider.append(travelTimeSliderHandle);
        this.options.travelTimeInfo.append(this.options.iconHTML).append(this.options.labelSpan).append(this.options.travelTimeSpan).append(unitSpan);

        $(this.options.travelTimeSlider).slider({
            range:  false,
            value:  that.options.initValue,
            min:    0,
            max:    that.options.maxValue,
            step:   that.options.step,
            
            slide: function (e, ui) {
                if ( ui.value == 0) return false;
                $(that.options.travelTimeSpan).text(ui.value);
            },
            stop: function(e, ui){
                var travelTimes = new Array()
                for(var i = 0; i < ui.value; i+= that.options.step)
                    travelTimes.push(that.options.travelTimes[i/that.options.step].time);
                that.options.onSlideStop(travelTimes);
                that.options.refreshUrlCallback();       
            }
        });
        this.onResize();

        /*
        prevent map click when clicking on slider
        */
        L.DomEvent.disableClickPropagation(this.options.sliderContainer);  

        return this.options.sliderContainer;
    },

    onResize: function(){
        if(this.options.map.getSize().x < 550){
            this.removeAndAddClass(this.options.miBox, 'leaflet-traveltime-slider-container-max', 'leaflet-traveltime-slider-container-min');
            this.removeAndAddClass(this.options.travelTimeInfo, 'travel-time-info-max', 'travel-time-info-min');
            this.removeAndAddClass(this.options.travelTimeSlider, 'leaflet-traveltime-slider-max', 'leaflet-traveltime-slider-min');
        }else{
            this.removeAndAddClass(this.options.miBox, 'leaflet-traveltime-slider-container-min', 'leaflet-traveltime-slider-container-max');
            this.removeAndAddClass(this.options.travelTimeInfo, 'travel-time-info-min', 'travel-time-info-max');
            this.removeAndAddClass(this.options.travelTimeSlider, 'leaflet-traveltime-slider-min', 'leaflet-traveltime-slider-max');
        }
    },

    removeAndAddClass: function(id,oldClass,newClass){
        $(id).addClass(newClass);
        $(id).removeClass(oldClass);
    },

    onSlideStop: function (onSlideStop) {
        var options = this.options;
        options.onSlideStop = onSlideStop;       
    },

    getValues : function() {
        var options = this.options;
        var travelTimes = new Array();
        for(var i = 0; i < $(this.options.travelTimeSlider).slider("value"); i+= options.step) {
            travelTimes.push(options.travelTimes[i/options.step].time);
        }
        return travelTimes;
    },

    getValue : function(){

        return $(this.options.travelTimeSlider).slider("value");
    }
 });

// L.Control.SliderControl = L.Control.extend({
//     options: {
//         position: 'topright',
//         maxValue: 90,
//         minValue: 0,
//         step: 15,
//         range: false,
//     },

//     initialize: function (options) {

//         L.Util.setOptions(this, options);
//         this.options.initValue = options.startTravelTime ? options.startTravelTime : 45;
//     },

//     onSlideStop: function (func){
//         this.options.slideStop = func;
//     },

//     onAdd: function (map) {
//         this.options.map = map;
//         map.on("resize", this.onResize.bind(this));

//         this.options.mapId = $(map._container).attr("id");
//         // Create a control sliderContainer with a jquery ui slider
//         var sliderContainer = L.DomUtil.create('div', 'slider', this._container);
//         $(sliderContainer).append(
//             '<div id="leaflet-traveltime-slider-container-'+this.options.mapId+'" class="leaflet-time-slider">' +
//                 '<div id="travel-time-info-'+this.options.mapId+'">'+
//                     '<span lang="en">Reisezeit: </span><span id="travel-time-'+ this.options.mapId + '">'+ this.options.initValue +'</span><span lang="de">min</span>'+
//                 '</div>' +
//                 '<div id="leaflet-traveltime-slider-'+this.options.mapId+'" class="no-border">' +
//                     '<div class="TT10" style="background-color: ' + _.findWhere(config.travelTimes, {time : 900 }).color + ';"></div>' +
//                     '<div class="TT20" style="background-color: ' + _.findWhere(config.travelTimes, {time : 1800 }).color + ';"></div>' +
//                     '<div class="TT30" style="background-color: ' + _.findWhere(config.travelTimes, {time : 2700 }).color + ';"></div>' +
//                     '<div class="TT40" style="background-color: ' + _.findWhere(config.travelTimes, {time : 3600 }).color + ';"></div>' +
//                     '<div class="TT50" style="background-color: ' + _.findWhere(config.travelTimes, {time : 4500 }).color + ';"></div>' +
//                     '<div class="TT60" style="background-color: ' + _.findWhere(config.travelTimes, {time : 5400 }).color + ';"></div>' +
//                     '<div class="ui-slider-handle"></div>' +
//                 '</div>' +
//             '</div>');
            
//         /*
//         prevent map click when clicking on slider
//         */
//         L.DomEvent.disableClickPropagation(sliderContainer);  

//         return sliderContainer;
//     },

//     onResize: function(){

//         if ( this.options.map.getSize().x < 400 ) {
            
//             this.removeAndAddClass("leaflet-traveltime-slider-container-" + this.options.mapId, 'leaflet-traveltime-slider-container-max', 'leaflet-traveltime-slider-container-min');
//             this.removeAndAddClass("travel-time-info-" + this.options.mapId, 'travel-time-info-max', 'travel-time-info-min');
//             this.removeAndAddClass("leaflet-traveltime-slider-" + this.options.mapId, 'leaflet-traveltime-slider-max', 'leaflet-traveltime-slider-min');
//         }
//         else {
//             this.removeAndAddClass("leaflet-traveltime-slider-container-" + this.options.mapId, 'leaflet-traveltime-slider-container-min', 'leaflet-traveltime-slider-container-max');
//             this.removeAndAddClass("travel-time-info-" + this.options.mapId, 'travel-time-info-min', 'travel-time-info-max');
//             this.removeAndAddClass("leaflet-traveltime-slider-" + this.options.mapId, 'leaflet-traveltime-slider-min', 'leaflet-traveltime-slider-max');
//         }
//     },

//     removeAndAddClass: function(id,oldClass,newClass){
//         $("#"+id).addClass(newClass);
//         $("#"+id).removeClass(oldClass);
//     },

//     startSlider: function (callback) {
//         var options = this.options;

//         $("#leaflet-traveltime-slider-" + this.options.mapId).slider({
//             range:  options.range,
//             value:  options.initValue,
//             min:    options.minValue,
//             max:    options.maxValue,
//             step:   options.step,
            
//             slide: function (e, ui) {
//                 if ( ui.value == 0) return false;
//                 $('#travel-time-' + options.mapId).text(ui.value);
//             },
//             stop: function(e, ui){
//                 var travelTimes = new Array()
//                 for(var i = options.minValue; i <= ui.value; i+= options.step)
//                     if(i!=0) travelTimes.push(i*60);
//                 options.slideStop(travelTimes);
//                 options.refreshUrlCallback();
//             }
//         });
//         this.onResize();
//     },

//      getValues : function() {
//         var options = this.options;
//         var travelTimes = new Array()
//         for(var i = options.minValue; i <= $("#leaflet-traveltime-slider-" + this.options.mapId).slider("value"); i+= options.step)
//             if(i!=0) travelTimes.push(i*60);
//         return travelTimes;
//     }
// });


/*
* IE 8 does not get the bind function. This is a workaround...
*/
if (!Function.prototype.bind) {
  Function.prototype.bind = function (oThis) {
    if (typeof this !== "function") {
      // closest thing possible to the ECMAScript 5 internal IsCallable function
      throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
    }

    var aArgs = Array.prototype.slice.call(arguments, 1), 
        fToBind = this, 
        fNOP = function () {},
        fBound = function () {
          return fToBind.apply(this instanceof fNOP && oThis
                                 ? this
                                 : oThis,
                               aArgs.concat(Array.prototype.slice.call(arguments)));
        };

    fNOP.prototype = this.prototype;
    fBound.prototype = new fNOP();

    return fBound;
  };
}

secToHoursMin = function (seconds) {

    var minutes = (seconds / 60).toFixed(0);
    var hours = Math.floor(minutes / 60);

    minutes = minutes - hours * 60;
    var timeString = "";

    if (hours != 0){
        timeString += (hours + "h "); 
    }

    timeString += (minutes + "min");
    return timeString;
};


getStartTime = function(){
        var jetzt = new Date();
        var Std = jetzt.getHours();
        var minutes = jetzt.getMinutes();
        minutes += Std * 60;
        var modulo = minutes%15

        minutes = minutes - modulo;

        return minutes;
}