var MI = {

	parsePolygons : function(polygonsJson, travelTimes) {
        
        var that = this;
        
        var polygons = new Array();

        if ( polygonsJson.error ) return errorMessage;

        _.each(polygonsJson["polygons"], function (polygonJson) {

            var polygon = new MI.Polygon();
            polygon.travelTime = polygonJson.traveltime;
            polygon.setOuterBoundary(that.parseLatLonArray(polygonJson.outerBoundary));
            polygon.setBoundingBox();

            _.each(polygonJson.innerBoundary, function (innerBoundary) {
                polygon.addInnerBoundary(that.parseLatLonArray(innerBoundary));
            });

             for(var i = 0; i < travelTimes.length; i++)
            	if(travelTimes[i].time == polygon.travelTime)
            		polygon.setColor(travelTimes[i].color);
            
            polygons.push(polygon);
        });

        return polygons;
    },

    Route : function(){

        var that = this;
        that.routeSegments = new Array();

        that.addRouteSegment = function(routeSegment){
            that.routeSegments.push(routeSegment);
        }

    },

    RouteSegment : function(){      

        this.polyLine = L.polyline([]);

        this.travelTime;
        this.length;
        this.elevationGain;  
        this.errorMessage;   

        this.transit;

    },

    parseRoutes : function(json){

        var routes = new Array();

        var i = 0;

        _.each(json.routes, function(route, index){

            routes[i] = new MI.Route();
            routes[i].travelTime = route.traveltime;

          _.each(route.segments, function(segment, index){

            var routeSegment = new MI.RouteSegment();
            routes[i].routeSegments.push(routeSegment);

            routes[i].routeSegments[index]      = segment;

            routes[i].routeSegments[index].polyLine = L.polyline([]);

            if(!routes[i].routeSegments[index].isTransit){
                routes[i].routeSegments[index].arrayZ = new Array();
                routes[i].routeSegments[index].arrayX = new Array();
            }

           

             var lastPoint;
             var dist = 0;
             _.each(segment.points, function(point) {
               
                var thisPoint = L.latLng(point[0], point[1]);



                routes[i].routeSegments[index].polyLine.addLatLng(thisPoint);

                if(point[2] != 0){

                    if(typeof point[2] != 'undefined'){
                        if(typeof lastPoint != 'undefined'){
                           dist += lastPoint.distanceTo(thisPoint);
                           routes[i].routeSegments[index].arrayX.push(dist);
                        }else{
                           routes[i].routeSegments[index].arrayX.push(dist);
                        }
                        routes[i].routeSegments[index].arrayZ.push(point[2]);
                    } 

                lastPoint = thisPoint;
                }
                

               

            });

        });

          i++;

        });

        return routes;
    },

    getAddressByCoordinates : function(latlng, language, callback){
        $.getJSON(MiConfig.nominatimUrl + 'reverse.php?&format=json&lat=' + latlng.lat + '&accept-language=' + language + '&lon=' + latlng.lng + '&json_callback=?', callback);
    },



    getRoutes : function(travelOptions, callback) {

        var source;
        var targets;
        var speed = 15;
        var uphill = 20;
        var downhill = -10;
        var time = 28800;
        var date = 20140331;

        /*
            as in getPolygons the travelOptions needs to be organised properly. Maybe an extra private function?
        */

        if(typeof travelOptions == 'undefined')
            alert('define travel options');
        else{
            if(typeof travelOptions.sources == 'undefined')
                alert("we need a source point");
            else
                sources = travelOptions.sources;

            if(typeof travelOptions.targets == 'undefined')
                alert("we need a target point");
            else
                targets = travelOptions.targets;

            if(typeof travelOptions.travelTimes != 'undefined')
                travelTimes = travelOptions.travelTimes;
            else
                travelTimes = MiConfig.defaultTravelTimeSliderOptions.travelTimes;
            if(typeof travelOptions.travelMode != 'undefined'){
                travelMode = travelOptions.travelMode;
            }               
            else
                travelMode = MiConfig.defaultTravelMode;

            if(typeof travelOptions.speed != 'undefined'){
                if(travelOptions.speed < 1){
                    alert("invalid paramters. speed needs to be higher")
                    return;
                }
                speed = travelOptions.speed;
            }
            if(typeof travelOptions.uphill != 'undefined'){
                uphill = travelOptions.uphill;
            }

            if(typeof travelOptions.downhill != 'undefined'){
                downhill = travelOptions.downhill;
            }

            if(typeof travelOptions.time != 'undefined'){
                time = travelOptions.time;
            }

            if(typeof travelOptions.date != 'undefined'){
                date = travelOptions.date;
            }

            if(uphill < 0 || downhill > 0 || uphill < -(downhill)){
                alert("wrong parameters for uphill and downhill")
                return;
            }
        }     

        // if there are no target points available, no routing is possible! 
        if ( sources.length != 0 && targets.length != 0 ) {

            var cfg = {};
            cfg.sources = [];
            _.each(sources, function(source){

                var src = {};
                src.id  = source.id;
                src.lat = source.getLatLng().lat;
                src.lon = source.getLatLng().lng;
                src.tm = {};

                /*
                src.tm[travelMode] = {};
                if(travelMode == "transit"){
                    src.tm.transit.frame = {};
                    src.tm.transit.frame.time = startTime * 60;
                    src.tm.transit.frame.date = date;
                }
                */


              src.tm[travelMode.type] = {};
            if(travelMode.type == "transit"){
               src.tm.transit.frame = {};
                src.tm.transit.frame.time = time;
                src.tm.transit.frame.date = date;
            }
            if(travelMode.type == "bike"){
                src.tm.bike.speed = speed;
                src.tm.bike.uphill = uphill;
                src.tm.bike.downhill = downhill;
            }
            if(travelMode.type == "walk"){
                src.tm.walk.speed = speed;
                src.tm.walk.uphill = uphill;
                src.tm.walk.downhill = downhill;
            }
                cfg.sources.push(src);



            });
            cfg.targets = [];
            _.each(targets, function(target){

                var trg = {};
                trg.id  = target.id;
                trg.lat = target.getLatLng().lat;
                trg.lon = target.getLatLng().lng;
                cfg.targets.push(trg);
            });

            $.getJSON(MiConfig.serviceUrl + MiConfig.serviceVersion + '/route?cfg=' +  encodeURIComponent(JSON.stringify(cfg)) + "&cb=?", callback);
        }
    },

	getTravelTimePolygons : function(travelOptions, callback) {

        var sources;
        var travelTimes;
        var travelMode;
        var speed = 15;
        var uphill = 20;
        var downhill = -10;

        var time = 28800;
        var date = 20140331;

        /*
        *	TODO reading the parameter from either default values or travelOptions needs to be done properly
        */


        if(typeof callback == 'undefined')
            alert('callback needs to be defined');
        if(typeof travelOptions == 'undefined')
            alert('define travel options');
        else{
            if(typeof travelOptions.sources == 'undefined')
                alert("we need a source point");
            else
                sources = travelOptions.sources;
            if(typeof travelOptions.travelTimes != 'undefined')
                travelTimes = travelOptions.travelTimes;
            else
                travelTimes = MiConfig.defaultTravelTimeSliderOptions.travelTimes;
            if(typeof travelOptions.travelMode != 'undefined'){
            	travelMode = travelOptions.travelMode;
            }               
            else
                travelMode = MiConfig.defaultTravelMode;

            if(typeof travelOptions.speed != 'undefined'){
                if(travelOptions.speed < 1){
                    alert("invalid paramters. speed needs to be higher")
                    return;
                }
                speed = travelOptions.speed;
            }

            if(typeof travelOptions.uphill != 'undefined'){
                uphill = travelOptions.uphill;
            }

            if(typeof travelOptions.downhill != 'undefined'){
                downhill = travelOptions.downhill;
            }

            if(uphill < 0 || downhill > 0 || uphill < -(downhill)){
                alert("wrong parameters for uphill and downhill")
                return;
            }

            if(typeof travelOptions.time != 'undefined'){
                time = travelOptions.time;
            }

            if(typeof travelOptions.date != 'undefined'){
                date = travelOptions.date;
            }

        }      

        /*
        TODO handling here is not nice. There need to be a better way to deal with different travelMode. Complex issue
        */  

        var times = new Array();

        for(var i = 0; i < travelTimes.length; i++){
            if(travelTimes[i].time > 7200){
                alert("invalid parameter: do not use times higher 7200");
                return;
            }
            times[i] = travelTimes[i].time;
        }
        	
   
        var cfg = {};
        cfg.polygon = { values : times };
        cfg.sources = [];
        _.each(sources, function(source){
            var src = {};
            src.id = source.id;
            src.lat = source.getLatLng().lat;
            src.lon = source.getLatLng().lng;
            src.tm = {};   

                   
            src.tm[travelMode.type] = {};
            if(travelMode.type == "transit"){
               src.tm.transit.frame = {};
                src.tm.transit.frame.time = time;
                src.tm.transit.frame.date = date;
            }
            if(travelMode.type == "bike"){
                src.tm.bike.speed = speed;
                src.tm.bike.uphill = uphill;
                src.tm.bike.downhill = downhill;
            }
            if(travelMode.type == "walk"){
                src.tm.walk.speed = speed;
                src.tm.walk.uphill = uphill;
                src.tm.walk.downhill = downhill;
            }
            

            


            cfg.sources.push(src);
        });
        
        $.getJSON(MiConfig.serviceUrl + MiConfig.serviceVersion + '/polygon?cfg=' + encodeURIComponent(JSON.stringify(cfg)) + "&cb=?", callback);
    },

    parseLatLonArray : function(latlngs) {

        var coordinates = new Array();

        _.each(latlngs, function (latlng) {
            coordinates.push(L.latLng(latlng[0], latlng[1]));
        });

        return coordinates;
    },


    Polygon : function(traveltime, outerBoundary) {

    var that = this;
    
    // default min/max values
    that.topRight         = new L.latLng(-90,-180);
    that.bottomLeft       = new L.latLng(90, 180);
    that.centerPoint      = new L.latLng(0,0);

    that.travelTime       = traveltime;
    that.color;
    that.outerBoundary    = outerBoundary;
    that.innerBoundaries  = new Array();

 
    that.setOuterBoundary = function(outerBoundary){
        that.outerBoundary = outerBoundary;
    }

  
    that.addInnerBoundary = function(innerBoundary){
        that.innerBoundaries.push(innerBoundary);
    }

    /**
     * @return {LatLngBounds} the leaflet bounding box
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getBoundingBox = function(){

        return new L.LatLngBounds(this._bottomLeft, this._topRight)
    }

    that.setBoundingBox = function() { 

        // calculate the bounding box
        _.each(this.outerBoundary, function(coordinate){

            if ( coordinate.lat > that.topRight.lat )   that.topRight.lat   = coordinate.lat;
            if ( coordinate.lat < that.bottomLeft.lat ) that.bottomLeft.lat = coordinate.lat;
            if ( coordinate.lng > that.topRight.lng )   that.topRight.lng   = coordinate.lng;
            if ( coordinate.lng < that.bottomLeft.lng ) that.bottomLeft.lng = coordinate.lng;
        });

        // precompute the polygons center
        that.centerPoint.lat = that.topRight.lat - that.bottomLeft.lat;
        that.centerPoint.lon = that.topRight.lon - that.bottomLeft.lon;
    }

    /**
     * Returns the center for this polygon. More precisly a gps coordinate
     * which is equal to the center of the polygons bounding box.
     * @return {latlng} gps coordinate of the center of the polygon
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getCenterPoint = function(){
        return that.centerPoint;
    },

    /**
     
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
     */
    that.getColor = function(){
        return that.color;
    }

    that.setColor = function(color){
    	that.color = color;
    }
},


/**
 *
 * @author Daniel Gerber <daniel.gerber@icloud.com>
 * @author Henning Hollburg <henning.hollburg@gmail.com>
 */
MultiPolygon : function(){
    
    var that = this;    

    that._topRight = new L.latLng(-90,-180);
    that._bottomLeft = new L.latLng(90, 180);
    that.travelTime;
    that.color;
    that.polygons = new Array();

    that.addPolygon = function(polygon){
        that.polygons.push(polygon);
    }

    that.setColor = function(color){
    	that.color = color;
    }

    that.getColor = function(){
    	return that.color;
    }

    that.getTravelTime = function(){
        return that.travelTime;
    }

    that.setTravelTime = function(travelTime){
        that.travelTime = travelTime;
    }

    that.getBoundingBox = function(){
        return new L.LatLngBounds(that._bottomLeft, that._topRight)
    }

    that.setBoundingBox = function(){
        for(var i = 0; i < that.polygons.length; i++){
            var poly = that.polygons[i];
            if(poly._topRight.lat > that._topRight.lat)
                that._topRight.lat = poly._topRight.lat;
            if(poly._bottomLeft.lat < that._bottomLeft.lat)
                that._bottomLeft.lat = poly._bottomLeft.lat;
            if(poly._topRight.lng > that._topRight.lng)
                that._topRight.lng = poly._topRight.lng;
            if(poly._bottomLeft.lng < that._bottomLeft.lng)
                that._bottomLeft.lng = poly._bottomLeft.lng;
        }
    }
},

/**
     *
     * @author Daniel Gerber <daniel.gerber@icloud.com>
     * @author Henning Hollburg <henning.hollburg@gmail.com>
 */
CustomPolygonLayer : L.Class.extend({
   
    initialize: function (options) {
    	this.opacity = MiConfig.defaultPolygonOptions.opacity;
		this.strokeWidth = MiConfig.defaultPolygonOptions.strokeWidth;
    	if(typeof options != 'undefined'){
    		if(typeof options.opacity != 'undefined')
    			this.opacity = options.opacity;
    		if(typeof options.strokeWidth != 'undefined')
    			this.strokeWidth = options.strokeWidth;
    	}       
        this._multiPolygons = new Array(); 
    },
  
    getBoundingBox : function(){
        return new L.LatLngBounds(this._bottomLeft, this._topRight)
    },
 
    onAdd: function (map) {

        this._map = map;
        // create a DOM element and put it into one of the map panes
        this._el = L.DomUtil.create('div', 'my-custom-layer-'+$(map._container).attr("id")+' leaflet-zoom-hide');
        $(this._el).css({"opacity": this.opacity});
        $(this._el).attr("id","canvas" + $(this._map._container).attr("id"));
        this._map.getPanes().overlayPane.appendChild(this._el);

        // add a viewreset event listener for updating layer's position, do the latter
        this._map.on('viewreset', this._reset, this);
        this._reset();
    },
  
    addLayer:function(polygonArray){        
      	this._resetBoundingBox();
        this._multiPolygons = new Array();
        for(var i = 0; i < polygonArray.length; i++){
                this._updateBoundingBox(polygonArray[i].outerBoundary);
                this._addPolygonToMultiPolygon(polygonArray[i]);
        }
        this._multiPolygons.sort(function(a,b) { return (b.getTravelTime() - a.getTravelTime()) });
        this._reset();
    },

    _addPolygonToMultiPolygon: function(polygon){
        for(var i = 0; i < this._multiPolygons.length; i++){
            if(this._multiPolygons[i].getTravelTime() == polygon.travelTime){
                this._multiPolygons[i].addPolygon(polygon);
                return;
            }
        }
        var mp = new MI.MultiPolygon();
        mp.setTravelTime(polygon.travelTime);
        mp.addPolygon(polygon);
        mp.setColor(polygon.getColor());
        this._multiPolygons.push(mp);
    },

    _resetBoundingBox: function(){
    	this._latlng = new L.LatLng(-180, 90);
        this._topRight = new L.latLng(-90,-180);
        this._bottomLeft = new L.latLng(90, 180);
    },
  
    _updateBoundingBox:function(CoordinateArray){
        for(var i = 0; i < CoordinateArray.length; i++){
            if(CoordinateArray[i].lat > this._topRight.lat) 
            	this._topRight.lat = CoordinateArray[i].lat;                
            else if(CoordinateArray[i].lat < this._bottomLeft.lat)  
            	this._bottomLeft.lat = CoordinateArray[i].lat;
               
            if(CoordinateArray[i].lng > this._topRight.lng)
                this._topRight.lng = CoordinateArray[i].lng;
            else if(CoordinateArray[i].lng < this._bottomLeft.lng)
                this._bottomLeft.lng = CoordinateArray[i].lng;
        }
        if(this._latlng.lat < this._topRight.lat)
            this._latlng.lat = this._topRight.lat;
        if(this._latlng.lng > this._bottomLeft.lng)
            this._latlng.lng = this._bottomLeft.lng;
    },
  
    onRemove: function (map) {
        // remove layer's DOM elements and listeners
        map.getPanes().overlayPane.removeChild(this._el);
        map.off('viewreset', this._reset, this);
    },
 
    _buildString:function(path, point, suffix){
        path += suffix + point.x + ' ' + point.y;
        return path;
    },
  
    _createSVGData: function(polygon){
        pathData ='';
        var point = this._map.latLngToLayerPoint(polygon[0]);
        pathData = this._buildString(pathData, point, 'M')
        for(var i = 1; i < polygon.length; i++){
            point = this._map.latLngToLayerPoint(polygon[i]);
            pathData = this._buildString(pathData, point, 'L')
        }
        pathData += 'z ';
        return pathData;
    },

    clearLayers: function(){
        $('#canvas'+ $(this._map._container).attr("id")).empty();
        this.initialize();
    },

    _reset: function () {
    	var that = this;

        if(this._multiPolygons.length > 0){
            var pos = this._map.latLngToLayerPoint(this._latlng);

            //internalSVGOffset is used to have a little space between geometries and svg frame. otherwise buffers won't be displayed at the edges...
            var internalSVGOffset = 100;
            pos.x -= internalSVGOffset;
            pos.y -= internalSVGOffset;
            L.DomUtil.setPosition(this._el, pos);

            //ie 8 and 9 
            if (navigator.appVersion.indexOf("MSIE 9.") != -1 )  {
                        $('#canvas'+ $(this._map._container).attr("id")).css("transform", "translate(" + pos.x + "px, " + pos.y + "px)");
            }
            if(navigator.appVersion.indexOf("MSIE 8.") != -1){
                        $('#canvas'+ $(this._map._container).attr("id")).css({"position" : "absolute"});
            }
            $('#canvas'+ $(this._map._container).attr("id")).empty();         

            var bottomLeft = this._map.latLngToLayerPoint(this._bottomLeft);
            var topRight = this._map.latLngToLayerPoint(this._topRight);
            var paper = Raphael('canvas'+ $(this._map._container).attr("id"), (topRight.x - bottomLeft.x) + internalSVGOffset * 2, (bottomLeft.y - topRight.y) + internalSVGOffset * 2);
            var st = paper.set();
            var svgData = "";
            var mp, poly;
            var svgDataArray = new Array();
            for(var i = 0; i < this._multiPolygons.length; i++){
                mp = this._multiPolygons[i];
                
                svgData = "";

                for(var j = 0; j < mp.polygons.length; j++){
                        poly = mp.polygons[j];
                        svgData += this._createSVGData(poly.outerBoundary);
                        for(var k = 0; k < poly.innerBoundaries.length; k++){
                            svgData += this._createSVGData(poly.innerBoundaries[k]);
                        }
                        var pointTopRight = this._map.latLngToLayerPoint(poly.topRight);
                        var pointBottomLeft = this._map.latLngToLayerPoint(poly.bottomLeft);
                    }
                    // ie8 (vml) gets the holes from smaller polygons
                    if(navigator.appVersion.indexOf("MSIE 8.") != -1){
                        if(i < this._multiPolygons.length-1){
                            for(var l = 0; l < this._multiPolygons[i+1].polygons.length; l++){
                                var poly2 = this._multiPolygons[i+1].polygons[l];
                                svgData += this._createSVGData(poly2.outerBoundary);
                            }
                        
                    }
                }


                var color = mp.getColor();
                var path = paper.path(svgData).attr({fill: color, stroke: color, "stroke-width": that.strokeWidth, "stroke-linejoin":"round","stroke-linecap":"round","fill-rule":"evenodd"})
                            .attr({"opacity":"0"}).animate({ "opacity" : "1" }, poly.travelTime/3)
                            path.translate((bottomLeft.x - internalSVGOffset) *-1,((topRight.y - internalSVGOffset)*-1));
                st.push(path);
            }

            if(navigator.appVersion.indexOf("MSIE 8.") != -1){
                $('shape').each(function() {
                    $( this ).css( {"filter": "alpha(opacity=" + that.opacity * 100 + ")"} );
                });
            }

        }
    }
}),

secondsToString : function(seconds){
      var hours = Math.floor(seconds/3600);
      var minutes = Math.floor(seconds/60)-hours*60;
      seconds = seconds - (hours * 3600) - (minutes *60);
      return hours+":"+ ("0" + minutes).slice(-2) +":"+ ("0" + seconds).slice(-2);
},

getStartTime : function(){
        var jetzt = new Date();
        var Std = jetzt.getHours();
        var minutes = jetzt.getMinutes();
        minutes += Std * 60;
        var modulo = minutes%15

        minutes = minutes - modulo;

        return minutes;
},


TravelStartDateControl : L.Control.extend({
    options: {
        position: 'topright',
        dateFormat: "yy-mm-dd"
    },

    initialize: function (options) {
        L.Util.setOptions(this, options);
    },

    onChange: function (func){
        this.options.onChange = func;
    },

    onAdd: function (map) {
        var that = this;
        that.options.map = map;
       
        var dateContainer = L.DomUtil.create('div', 'startDatePicker', this._container);

        that.datepicker = $('<div/>');

        $(dateContainer).append(that.datepicker);

        $(that.datepicker).datepicker({
            onSelect: function(e, ui){
                that.options.onChange(that.getValue());
            }
        });    

        L.DomEvent.disableClickPropagation(dateContainer);         
       
        return dateContainer;
    },

    getValue : function() {   
        var that = this;
        var date = $(that.datepicker).datepicker({ dateFormat: 'dd-mm-yy' }).val()
        var splitDate = date.split('/');
        var yyyymmdd = splitDate[2] + '' + splitDate[0] + '' + splitDate[1];
        return yyyymmdd;
    }
}),


TravelStartTimeControl : L.Control.extend({
    options: {
        position: 'topright',
        range: false,
        min: 0,
        max: 1440,
        step: 15,
        initValue: 480,
        value: 0,
    },

    initialize: function (options) {
        this.options.value = MI.getStartTime();
        L.Util.setOptions(this, options);
    },

    onSlideStop: function (func){
        this.options.slideStop = func;
    },

    minToString: function(minutes){
        var hours = Math.floor(minutes / 60);
        var min = minutes - (hours * 60);
        if(hours > 24) hours -= 24;
        if(hours.length == 1) hours = '0' + hours;
        if(min.length == 1) min = '0' + min;
        if(min == 0) min = '00';
        return(hours+':'+min);
    },

    onAdd: function (map) {

        var that = this;

        that.options.map = map;
        that.options.mapId = $(map._container).attr("id");


        map.on("resize", this.onResize.bind(this));
        // Create a control sliderContainer with a jquery ui slider
        var sliderContainer = L.DomUtil.create('div', 'startTimeSlider', this._container);

        that.miBox = $('<div/>', {"class" : "mi-box"});
        that.startTimeInfo = $('<div/>');
        that.label = $('<span/>');
        that.slider = $('<div/>');

        $(sliderContainer).append(that.miBox.append(that.startTimeInfo.append(that.label)).append(that.slider))

        $(that.label ).text( 'Abfahrt: '+ that.minToString(this.options.value) +' - ' + that.minToString(this.options.value + 60)+ ' Uhr');

        $(that.slider).slider({
            range:  that.options.range,
            value:  that.options.value,
            min:    that.options.min,
            max:    that.options.max,
            step:   that.options.step,
            
            slide: function (e, ui) {
                $(that.label ).text( 'Abfahrt: '+ that.minToString(ui.value) +' - ' + that.minToString(ui.value + 60)+ ' Uhr');
                that.options.value = ui.value;
            },
            stop: function(e, ui){
                that.options.slideStop(ui.value);
            }
        });
    
        this.onResize();
       /*
        prevent map click when clicking on slider
        */
        L.DomEvent.disableClickPropagation(sliderContainer);  

        return sliderContainer;
    },

    onResize: function(){
        if(this.options.map.getSize().x < 550){
            this.removeAndAddClass(this.miBox, 'leaflet-traveltime-slider-container-max', 'leaflet-traveltime-slider-container-min');
            this.removeAndAddClass(this.startTimeInfo, 'travel-time-info-max', 'travel-time-info-min');
            this.removeAndAddClass(this.slider, 'leaflet-traveltime-slider-max', 'leaflet-traveltime-slider-min');
        }else{
            this.removeAndAddClass(this.miBox, 'leaflet-traveltime-slider-container-min', 'leaflet-traveltime-slider-container-max');
            this.removeAndAddClass(this.startTimeInfo, 'travel-time-info-min', 'travel-time-info-max');
            this.removeAndAddClass(this.slider, 'leaflet-traveltime-slider-min', 'leaflet-traveltime-slider-max');
        }
    },

    removeAndAddClass: function(id,oldClass,newClass){
        $(id).addClass(newClass);
        $(id).removeClass(oldClass);
    },

    getValue : function() {    
        return this.options.value;
    }
}),

/*currently just used as info button. Should be implemented a bit more flexible*/

 Button : L.Control.extend({
    initialize: function(options){
        this.options = MiConfig.defaultButtonOptions;
        if(typeof options != 'undefined'){
            if(typeof options.icon != 'undefined'){
                this.options.icon = options.icon;
            }
            if(typeof options.position != 'undefined'){
                this.options.position = options.position;
            }
             if(typeof options.label != 'undefined'){
                this.options.label = options.label;
            }
        }

        L.Util.setOptions(this);
    },
    onAdd: function(map){
        var that = this;
        var buttonContainer = L.DomUtil.create('div', this._container);
        var button = $('<button/>', {text: that.options.label});
        this.options.button = button; 
        $(buttonContainer).append(button);

        /* TODO we should allow some more options here*/
        $(button).button({
            icons: {
                primary: that.options.icon
            },
            text: false}
        );

        L.DomEvent.disableClickPropagation(buttonContainer);

        return buttonContainer;
    },
    onClick: function(onClick){
        $(this.options.button).click(onClick);
    }
 }),

NamePicker : L.Control.extend({  

    initialize: function(options){

            this.options = MiConfig.defaultNamePickerOptions;

           
            if(typeof options != "undefined"){
                if(typeof options.position != "undefined")
                    this.options.position = options.position;
                if(typeof options.label != "undefined")
                    this.options.label = options.label;
                if(typeof options.country != "undefined")
                    this.options.country = options.country;
                if(typeof options.reset != "undefined")
                    this.options.reset = options.reset;
                if(typeof options.reset != "undefined")
                    this.options.reverse = options.reverse;
                if(typeof options.placeholder != "undefined")
                    this.options.placeholder = options.placeholder;
            }

            L.Util.setOptions(this);

    },

    onAdd: function(map){
        var that = this;
        var countrySelector =  "";

        var nameContainer = L.DomUtil.create('div', this._container);
        var miBox = $('<div/>', {"class" : "mi-box"});

        that.options.input = $('<input/>', {"placeholder" : that.options.placeholder, "onclick" : "this.select()"});
        that.options.map = map;
        map.on("resize", this.onResize.bind(this));          

        $(nameContainer).append(miBox);        
        $(miBox).append(that.options.input);

        if(that.options.reset){
            that.options.resetButton = $('<button/>', {"text": "reset", "style": "width:22px; height:22px; bottom: 2px;"});

            $(miBox).append(that.options.resetButton);    

            $(this.options.resetButton).button({
                icons: {
                    primary: "ui-icon-close"
                },
            text: false}
            );
        }        

        if(that.options.reverse){
            that.options.reverseButton = $('<button/>', {"text": "reverse", "style": "width:22px; height:22px; bottom: 2px;"});

            $(miBox).append(that.options.reverseButton);    

            $(this.options.reverseButton).button({
                icons: {
                    primary: "ui-icon-transferthick-e-w"
                },
            text: false}
            );
        }        
         
       
        L.DomEvent.disableClickPropagation(nameContainer);         

        if(typeof that.options.country != 'undefined')
            countrySelector += " AND country:" + that.options.country;

        $(that.options.input).autocomplete({

           

            source: function( request, response ) {

                that.source = this;

                var requestElements = request.term.split(" ");
                var numbers = new Array();
                var requestString = "";
                var numberString = "";
                for(var i = 0; i < requestElements.length; i++){
                    if(requestElements[i].search(".*[0-9].*") != -1)
                        numbers.push(requestElements[i]);
                    else
                        requestString += requestElements[i] + " ";
                }

                if(numbers.length > 0){
                    numberString += " OR ";
                    for(var j = 0; j < numbers.length; j++){
                        var n = "(postcode : " + numbers[j] + " OR housenumber : " + numbers[j] + " OR street : " + numbers[j] + ") ";
                        numberString +=  n;
                    }
                }


                delay: 150,

                $.ajax({
                  url: that.options.serviceUrl, 
                  dataType: "jsonp",
                  jsonp: 'json.wrf',
                  data: {
                    wt:'json',
                    indent : true,
                    rows: 10,
                    qt: 'en',
                    q:  "(" + requestString + numberString + ")" + countrySelector
                  }, 
                  success: function( data ) {

                    var pastArrays = new Array();
                    response( $.map( data.response.docs, function( item ) {

                        if(item.osm_key == "boundary")
                            return;

                        var latlng = item.coordinate.split(',');
                        var place           = {};
                        var firstRow        = [];
                        var secondRow       = [];
                        place.name          = item.name;
                        place.city          = item.city;
                        place.street        = item.street;
                        place.housenumber   = item.housenumber;
                        place.country       = item.country;
                        place.postalCode    = item.postcode;
                        if (place.name)       firstRow.push(place.name);
                        if (place.city)       firstRow.push(place.city);
                        if (place.street)     secondRow.push(place.street);
                        if (place.housenumber) secondRow.push(place.housenumber);
                        if (place.postalCode) secondRow.push(place.postalCode);
                        if (place.city)       secondRow.push(place.city);

                        /*only show country if undefined*/
                        if(typeof that.options.country == 'undefined')
                            if (place.country)    secondRow.push(place.country);

                        /*if same looking object is in list already: return*/
                        for(var i = 0; i < pastArrays.length; i++)
                            if(pastArrays[i] == ""+firstRow.join()+secondRow.join())
                                return;

                        pastArrays.push(""+firstRow.join()+secondRow.join());

                      return {
                        label: firstRow.join(", "),
                        value: firstRow.join(", "),
                        firstRow: firstRow.join(", "),
                        secondRow: secondRow.join(" "),
                        latlng: new L.LatLng(latlng[0], latlng[1])
                      }
                    }));
                  }
                });
              },
              minLength: 1,
              select: function( event, ui ) {
                that.options.value = ui.item;
                that.options.onEnter(ui.item);
              },
              open: function() {
                $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
              },
              close: function() {
                $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
              },
               create: function() {
                $( this ).addClass( "ui-corner-all" );
             }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            /*Styling in two rows would be nice. Looks ugly in JQuery though*/
            return $( "<li>" )
                .append( "<a><span class='address-row1'>"+ item.firstRow + "</span><span class='address-row2'>  " + item.secondRow + "</span></a>" )
                .appendTo( ul );
            };
            this.onResize();     

        return nameContainer;
    },

    onReset: function(onReset){
        var that = this;   

        $(this.options.resetButton).click(onReset);
        $(this.options.resetButton).click(function(){
            $(that.options.input).val("");
        });
    },

    onReverse: function(onReverse){
       var that = this;  
       $(this.options.reverseButton).click(onReverse);
    },

    onResize: function(){
        var that = this;
        if(this.options.map.getSize().x < 550){
            $(that.options.input).css({'width':'45px'});
        }else{
            $(that.options.input).css({'width':''});
        }
    },

    onEnter: function(onEnter){
        var that = this;
        that.options.onEnter = onEnter;       
    },

    setFieldValue : function(val){
         $(this.options.input).val(val);
    },

    getFieldValue : function(){
        return $(this.options.input).val();
    },

    getValue : function(){
        return this.options.value;
    }

}),

/**
*
* @author Daniel Gerber <daniel.gerber@icloud.com>
* @author Henning Hollburg <henning.hollburg@gmail.com>
*/


 SliderControl : L.Control.extend({
   
    initialize: function (sliderOptions) {
        
        this.options = MiConfig.defaultTravelTimeSliderOptions;
        if(typeof sliderOptions != "undefined"){
            if(typeof sliderOptions.position != "undefined")
                this.options.position = sliderOptions.position;
            if(typeof sliderOptions.initValue != "undefined")
                this.options.initValue = sliderOptions.initValue;
            if(typeof sliderOptions.label != "undefined")
                this.options.label = sliderOptions.label;
            if(typeof sliderOptions.travelTimes != "undefined")
                this.options.travelTimes = sliderOptions.travelTimes;
            if(typeof sliderOptions.icon != 'undefined')
                this.options.icon = sliderOptions.icon;
        }
        this.options.maxValue = MiConfig.defaultTravelTimeSliderOptions.travelTimes[MiConfig.defaultTravelTimeSliderOptions.travelTimes.length-1].time/60;
        this.options.step = (MiConfig.defaultTravelTimeSliderOptions.travelTimes[1].time - MiConfig.defaultTravelTimeSliderOptions.travelTimes[0].time)/60;
        L.Util.setOptions(this);
    },

    onAdd: function (map) {
        var that = this;
        this.options.map = map;
        map.on("resize", this.onResize.bind(this));          

        var sliderColors = "";
        var percent = 100 / this.options.travelTimes.length;
        for(var i = 0; i < this.options.travelTimes.length; i++){
            if(i == 0)
                sliderColors += '<div style="position: absolute; top: 0; bottom: 0; left: ' + i * percent + '%; right: ' + (100 - (i + 1)* percent )+ '%; background-color: ' + this.options.travelTimes[i].color + '; -moz-border-top-left-radius: 8px;-webkit-border-radius-topleft: 8px; border-top-left-radius: 8px; -moz-border-bottom-left-radius: 8px;-webkit-border-radius-bottomleft: 8px; border-bottom-left-radius: 8px;"></div>';
            else if(i < this.options.travelTimes.length -1)
                sliderColors += '<div style="position: absolute; top: 0; bottom: 0; left: ' + i * percent + '%; right: ' + (100 - (i + 1)* percent )+ '%; background-color: ' + this.options.travelTimes[i].color + ';"></div>';
            else if(i == this.options.travelTimes.length -1)
                sliderColors += '<div style="position: absolute; top: 0; bottom: 0; left: ' + i * percent + '%; right: ' + (100 - (i + 1)* percent )+ '%; background-color: ' + this.options.travelTimes[i].color + '; -moz-border-top-right-radius: 8px;-webkit-border-radius-topright: 8px; border-top-right-radius: 8px; -moz-border-bottom-right-radius: 8px;-webkit-border-radius-bottomright: 8px; border-bottom-right-radius: 8px;"></div>';
        }

        // Create a control sliderContainer with a jquery ui slider
        this.options.sliderContainer = L.DomUtil.create('div', this._container);

        this.options.miBox = $('<div/>', {"class" : "mi-box"});
        this.options.travelTimeInfo = $('<div/>');
        this.options.travelTimeSlider = $('<div/>', {"class" : "no-border"}).append(sliderColors);
        var travelTimeSliderHandle = $('<div/>', {"class" : "ui-slider-handle"});
        this.options.labelSpan = $('<span/>', {"text" : this.options.label + " "});

        if(this.options.icon != 'undefined')
            this.options.iconHTML = $('<img/>', {"src" : this.options.icon})


        this.options.travelTimeSpan = $('<span/>', {"text" : this.options.initValue });
        var unitSpan = $('<span/>', {"text" : "min"});

        $(this.options.sliderContainer).append(this.options.miBox);
        this.options.miBox.append(this.options.travelTimeInfo);
        this.options.miBox.append(this.options.travelTimeSlider);
        this.options.travelTimeSlider.append(travelTimeSliderHandle);
        this.options.travelTimeInfo.append(this.options.iconHTML).append(this.options.labelSpan).append(this.options.travelTimeSpan).append(unitSpan);



        $(this.options.travelTimeSlider).slider({
            range:  false,
            value:  that.options.initValue,
            min:    0,
            max:    that.options.maxValue,
            step:   that.options.step,
            
            slide: function (e, ui) {
                if ( ui.value == 0) return false;
                $(that.options.travelTimeSpan).text(ui.value);
            },
            stop: function(e, ui){
                var travelTimes = new Array()
                for(var i = 0; i < ui.value; i+= that.options.step)
                    travelTimes.push(that.options.travelTimes[i/that.options.step]);
                that.options.onSlideStop(travelTimes);
            }
        });
        this.onResize();

        /*
        prevent map click when clicking on slider
        */
        L.DomEvent.disableClickPropagation(this.options.sliderContainer);  

        return this.options.sliderContainer;
    },

    onResize: function(){
        if(this.options.map.getSize().x < 550){
            this.removeAndAddClass(this.options.miBox, 'leaflet-traveltime-slider-container-max', 'leaflet-traveltime-slider-container-min');
            this.removeAndAddClass(this.options.travelTimeInfo, 'travel-time-info-max', 'travel-time-info-min');
            this.removeAndAddClass(this.options.travelTimeSlider, 'leaflet-traveltime-slider-max', 'leaflet-traveltime-slider-min');
        }else{
            this.removeAndAddClass(this.options.miBox, 'leaflet-traveltime-slider-container-min', 'leaflet-traveltime-slider-container-max');
            this.removeAndAddClass(this.options.travelTimeInfo, 'travel-time-info-min', 'travel-time-info-max');
            this.removeAndAddClass(this.options.travelTimeSlider, 'leaflet-traveltime-slider-min', 'leaflet-traveltime-slider-max');
        }
    },

    removeAndAddClass: function(id,oldClass,newClass){
        $(id).addClass(newClass);
        $(id).removeClass(oldClass);
    },

    onSlideStop: function (onSlideStop) {
        var options = this.options;
        options.onSlideStop = onSlideStop;

        
    },

    getValues : function() {
        var options = this.options;
        var travelTimes = new Array()
        for(var i = 0; i < $(this.options.travelTimeSlider).slider("value"); i+= options.step)
        	travelTimes.push(options.travelTimes[i/options.step]);
        return travelTimes;
    }
 }),


RadioButtonControl : L.Control.extend({
  

  initialize: function (options) {

  	this.options = MiConfig.defaultRadioOptions;

    if(typeof options != 'undefined'){
        if(typeof options.position != 'undefined')
            this.options.position = options.position;
        if(typeof options.buttons != 'undefined'){
            this.options.buttons = options.buttons;
        }else
            alert("give some button names and values")
        if(typeof options.checked != 'undefined')
            this.options.checked = options.checked;
        else{
        	this.options.checked = this.options.buttons[0].value;
        }
    }
    L.Util.setOptions(this,options);
  },

  onAdd: function (map) {
    var that = this;

    this.options.map = map;
    var travelContainer = L.DomUtil.create('div', this._container);
    this.options.input = this.getRadioButtonHTML();
    $(travelContainer).append(this.options.input);

    $(this.options.input).buttonset({}).change(function(){
        that.options.checked = $("input[name='radio_travelSpeed_" + that.options.randomSetId + "']:checked").val();
        that.options.onChange(that.options.checked);
    });  

    $(this.options.input).each(function(){
        $( "[title]" ).tooltip();   
    }); 

    // prevent map click when clicking on slider
    
    L.DomEvent.addListener(travelContainer, 'click', L.DomEvent.stopPropagation);
    return travelContainer;
  },

  onChange: function (func){
        var that = this; 
        that.options.onChange = func;       
       
  },

  getValue: function(){
      return this.options.checked;
  },

  getRadioButtonHTML: function(){
        var that = this; 

        var div = $('<div/>');

        that.options.randomSetId = Math.random();
         
        for(var i = 0; i < that.options.buttons.length; i++){

            /*dirty hack for js stupidity*/
            that.options.randomId = Math.random();

            var input = $('<input/>', {"type": 'radio', "id" : 'MI_Radio' + that.options.randomId, "value": that.options.buttons[i].value, "name" : 'radio_travelSpeed_' + that.options.randomSetId});
            var label = $('<label/>', {"for": 'MI_Radio' + that.options.randomId, "text" : that.options.buttons[i].label});

            var checked = '';
            var tooltip = '';
            if(that.options.buttons[i].value == that.options.checked) 
                input.attr({"checked" : "checked"});
            if(typeof that.options.buttons[i].tooltip != 'undefined') 
                label.attr({"title" : that.options.buttons[i].tooltip});

            div.append(input);
            div.append(label);
     }  
    return div;
   }
})

}

/*
* IE 8 does not get the bind function. This is a workaround...
*/
if (!Function.prototype.bind) {
  Function.prototype.bind = function (oThis) {
    if (typeof this !== "function") {
      // closest thing possible to the ECMAScript 5 internal IsCallable function
      throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
    }

    var aArgs = Array.prototype.slice.call(arguments, 1), 
        fToBind = this, 
        fNOP = function () {},
        fBound = function () {
          return fToBind.apply(this instanceof fNOP && oThis
                                 ? this
                                 : oThis,
                               aArgs.concat(Array.prototype.slice.call(arguments)));
        };

    fNOP.prototype = this.prototype;
    fBound.prototype = new fNOP();

    return fBound;
  };
}