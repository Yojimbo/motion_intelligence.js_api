# Motion Intelligence API
This open source API is used to interact with the webservices provided by the Motion Intelligence GmbH (from now on called MI). Please note that, while this library is free of  charge the webservices per se are not. The service can be used freely for a limited amount of queries per month. If you want to integrate our API please free to contact us at <contact@motionintelligence.net> or visit our [website](motionintelligence.net).

![Demo if the integration of the MI API into an existing leaflet map.](https://photos-1.dropbox.com/t/0/AADdj3uubeAbHQWhBR2313s1gq7Odtx85PujayTxTN8KMw/12/4424448/png/2048x1536/3/1390582800/0/2/Screenshot%202014-01-24%2016.47.54.png/4sTucl-QZXSS3hXTDisofTMPScbupUvWe_BmJEfcveI)

## Configuration
In order to properly configure the API you first need to include the config file into your HTML template. This can be done by including `<script src="path/to/config.js"></script>`. Please note that you need to copy the config.js.default file to config.js first. You could also copy & paste the contents of the file into you template (and safe the time for loading the file).

Let's have a look at the parameters:

* `serviceUrl`: This is the URL of the webserver MI is providing for you. The service requests are send there. 
* `serviceVersion`: This is going to be **v1** for quiet some time. 
* `mapDiv`: The id of the div where the map is going to be inserted.
* `initZoom`, `minZoom`, `maxZoom`: the initial (if no source points are provided), minimal and maximal zoom levels. Zoom levels range from 0 to 19. For further information please visit the [OSM Wiki](http://wiki.openstreetmap.org/wiki/Zoom_levels).
* `attribution`: This is the attribution string displayed in the lower right side of the map. This always needs to contain `Realised by <a href="http://motionintelligence.net">Motion Intelligence`, `Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>` and an attribution to the service which provides the tiles. At the moment we support [Cloudmate](http://cloudmade.com/) and [MapBox](https://www.mapbox.com/).
* `gpsStartPoint`: If no source points are given, then the map is centered at this GPS coordinate. If there are sources added to the map (see `defaultMarkers`), the map is centered in a way that all source points are visible.
* `defaultMarkers`: This is a set of default markers which are placed on the map automatically. These markers should be used to display a first result to the user, no user interaction required.
* `addTravelTimeSlider`: Set this flag to integrate the travel time slider as a leaflet control plugin in the top right corner. If you do not provide this slider, the time values of `travelTimes` will be used to generate the travel time polygons.
* `clickMapForRoutes`: This is a very experimental feature. If enabled a right click on the map triggers the insertion of a marker on the map and a RoutingService request for all source points to all target points. Note that this are m * n routing request, which might take some time.
* `travelTimes`: These are the values shown in the travel time slider. Each value, represented in seconds, has a color specified. This is typically from green to red, e.g. from short distance to far distance. Use this to create a color scheme which fits to your website. Please note that the travel times, for now, need to be in 10 minute intervals.
* `sliderDefault`: The default value for the travel time slider, represented in minutes. There has to be a corresponding entry in `travelTimes`.
* `cloudmade|mapbox.active`: set one the values to true, to active the tile layer service.
* `cloudmade|mapbox.mapStyle`: You can generate your own custom maps or use predefined ones at both MapBox and Cloudmate. You need to register there and select a map style. 
* `cloudmade.apiKey`: This key is only needed for cloudmade. Register to get one. 

## Services

### PolygonService

### RouteService

## Example

### Create new map

### Integrate with exiting map


## FAQ

1. Can I use the Motion Intelligence API with another mapping library such as OpenLayers or Google Maps?
    * We are currently only supporting Leaflet. We want to port this library to different mapping libraries as soon as possible. Any help is kindly appreciated!